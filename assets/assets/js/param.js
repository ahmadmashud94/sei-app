$(document).ready(function(){


	$(document).on('blur', '[name=value]',function(){
		this.value = this.value != '' ? 'Rp '+accounting.formatMoney(this.value,"",0,".","") : '';
	});

	$(document).on('focus', '[name=value]',function(){
		this.value = this.value != '' ? accounting.unformat(this.value,',') : '';
	});


});
