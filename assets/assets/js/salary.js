
$(document).ready(function() {
	$("#id-form").each(function(){
		var currentForm = $(this);

		$(this).validate({
			rules: {
				salary_type : "required" ,
				period : "required" ,
				salary_file : "required" 
			},
			submitHandler: function(form) {
				submit();
			}
		});
	});

});