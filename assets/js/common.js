	
$("#dataTable").dataTable( {
	 "scrollY": 300,
        "paging": false,
	"autoWidth": true,
	"scrollX": true,
	"bLengthChange": false,
	"bFilter": true,
	"bInfo": false,
	"columnDefs": [{
		"targets": '_all',
		"createdCell": function (td, cellData, rowData, row, col) {
			$(td).css('padding', '10px')
		}
	}],
	"initComplete": function (settings, json) {  
		$(".dataTables").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");            
	},
});


function deleteDelimiterMultipleInput(parent, input){
    $(parent).find(input).each(function(){
        var price = $(this).val();
        $(this).val(accounting.unformat(price));
    });
}