
<!DOCTYPE html>
<html>
<head>
	<title>Page Title</title>
</head>
<style>
*{
	font-family: sans-serif;
	margin: 5px;
	
} 
table td{
	white-space: nowrap;
}
.div-out{
	padding: 0px;
	border: 1px solid black;
	margin: 50px;
}
.custom-table, .custom-table th, .custom-table td {
	border: 1px solid black;
	border-collapse: collapse;text-align:center;
	margin: 0px;
}

.custom-table {
	width: 100%;
} 
.custom-table tr td:nth-child(1)  {
	text-align: left;
	border-right: none;
	padding-left: 7px;
}
.custom-table tr td:nth-child(2)  {
	border-left: none;
	text-align: left;
}
.custom-table tr td:nth-child(3)  {
	text-align: left;
	border-right: none;
	padding-left: 7px;
}
.custom-table tr td:nth-child(4)  {
	text-align: left;
	border-right: none;
	padding-left: 7px;
}

.custom-table2 tr td {
	width: 100%;
	border: none;
} 
.custom-table tr td:nth-child(3)  {
	text-align: right;
}
.custom-table tr td:nth-child(2)  {
	text-align: center;
}
</style>
<body>
	
<!-- 	<div style="text-align:center">
		<h2>GAJI <?php echo getMonthName($data->bulan)." ".$data->tahun; ?></h2>
	</div>
	<div >
		<table width="100%"  style="text-align:center;font-size: 20px;">
			<tr>
				<td colspan="2">Koperasi</td>
				<td colspan="2">Cuti Karyawan</td>
			</tr>
			<tr>
				<td>Sisa saldo<?php echo $data->saldo_tabungan; ?></td>
				<td>Sisa Pinjamannull</td>
				<td>Sisa Sisa Cutinull Hari</td>
				<td></td>
			</tr>
		</table>
	</div> -->
 
	<?php 
	$total = $data->gaji_pokok + $data->tunjangan_karyawan +
	$data->tunjangan_jabatan + $data->tunjangan_masa_kerja;
	$total_penerimaan = $total  + $data->thr + $data->tunjangan_kehadiran_transport + $data->tunjangan_shift + $data->insentif_kehadiran + $data->pph21_dtp + $data->rapel + $data->sisa_cuti + $data->bonus + $data->uang_makan_rp;
	$total_potongan = $data->potongan_absensi + $data->simpanan_koperasi + 
	$data->pinjaman_koperasi + $data->iuran_jht + $data->iuran_pensiun + $data->bpjs_kesehatan + $data->pph21 + $data->angsuran_pinjaman ;
	$total_penerimaan_potongan = $total_penerimaan - $total_potongan;
	$take_home_pay = $total_penerimaan_potongan + $data->overtime;
	?>

	<div class="div-out" align="center">
		<table align="center" class="custom-table" width="30%"  style="font-size: 14px;">
			<tr>
				<td colspan="2">
					<img width="1000%" height="200%" src="<?php echo base_url(); ?>assets/images/logo.png">
					
				</td>
				<td style="border-right: none;">
				</td>
				<td style="border-left: none;">
					<table class="custom-table" align="right" style="font-size: 10px;font-weight: bold;width: 90%;">
						<tr>
							<td ><center><h2>SLIP GAJI</h2></center></td>
						</tr>
						<tr>
							<td ><center><h4>PRIBADI DAN RAHASIA</h4></center></td>
						</tr>
						<tr>
							<td><center>Periode: <?php echo getMonthName($data->bulan)." ".$data->tahun; ?></center></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="white-space: initial;border-bottom: none" colspan="4"> Nama  &nbsp;:  &nbsp; <b><?php echo $data->nama; ?></b> </td>
			</tr>
			<tr>
				<td style="white-space: initial;border-top: none;" colspan="4"> Status  &nbsp;:  &nbsp; <b><?php echo $data->status; ?></b> </td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;"><b>PENERIMAAN</b></td>
				<td colspan="2"><b>POTONGAN</b></td>
			</tr>
			<tr>
				<td colspan="2">
					<table  class="custom-table2" width="100%" border="0">
						<tr>
							<td> GP</td>
							<td> : Rp</td>
							<td  colspan="3"><?php echo toFormatMoney($data->gaji_pokok); ?></td>
						</tr>
						<tr>
							<td> Tunjangan Karyawan </td>
							<td> : Rp</td>	
							<td  colspan="3"><?php echo toFormatMoney($data->tunjangan_karyawan); ?></td>
						</tr>
						<tr>
							<td>Tunjangan Jabatan </td>
							<td> : Rp</td>
							<td  colspan="3"><?php echo toFormatMoney($data->tunjangan_jabatan); ?></td>
						</tr>
						<tr>
							<td> Tunjangan Masa Kerja </td>
							<td> : Rp</td>
							<td  colspan="3"><?php echo toFormatMoney($data->tunjangan_masa_kerja); ?></td>
						</tr>
						<tr>
							<td style="border-top: 1px solid black;"> <b>Total</b> </td>
							<td style="border-top: 1px solid black;"> : Rp</td>
							<td style="border-top: 1px solid black;" colspan="3"><?php echo toFormatMoney($total ); ?></td>
						</tr>
						<!-- batas -->
						<tr>
							<td style="border-top: 1px solid black;"> Absensi Kehadiran/ Transfort </td>
							<td style="border-top: 1px solid black;"> : Rp</td>
							<td style="border-top: 1px solid black;" colspan="3"><?php echo toFormatMoney($data->tunjangan_kehadiran_transport); ?></td>
						</tr>

						<tr>
							<td> Tunjangan Shift </td>
							<td> : Rp</td>
							<td  colspan="3"><?php echo toFormatMoney($data->tunjangan_shift); ?></td>
						</tr>
						<tr>
							<td>Insentif Kehadiran </td>
							<td> : Rp</td>
							<td  colspan="3"><?php echo toFormatMoney($data->insentif_kehadiran); ?></td>
						</tr>


						<?php if ($data->sisa_cuti != null ) {?>
							<tr>
								<td>Sisa Cuti <?php echo ($data->tahun)-1; ?></td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->sisa_cuti); ?></td>
							</tr>
						<?php } ?>


						<?php if ($data->rapel != null ) {?>
							<tr>
								<td>Rapel Kenaikan Gaji</td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->rapel); ?></td>
							</tr>
						<?php } ?>

						<?php if ($data->thr != null ) {?>
							<tr>
								<td>THR</td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->thr); ?></td>
							</tr>
						<?php } ?>

						<?php if ($data->bonus != null ) {?>
							<tr>
								<td>Bonus</td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->bonus); ?></td>
							</tr>
						<?php } ?>

						<?php if ($data->uang_makan_rp != null ) {?>
							<tr>
								<td>Uang Makan</td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->uang_makan_rp); ?></td>
							</tr>
						<?php } ?>

						<?php if ($data->pph21_dtp != null ) { ?>
							<tr>
								<td> PPH Ditanggung Pemerintah </td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->pph21_dtp); ?></td>
							</tr>
						<?php } ?>

						<?php if ($data->lain_lain != null ) {?>
							<tr>
								<td>Lain-lain</td>
								<td> : Rp</td>
								<td  colspan="3"><?php echo toFormatMoney($data->lain_lain); ?></td>
							</tr>
						<?php } ?>
						
						<tr>
							<td style="border-bottom: 1px solid black;border-top: 1px solid black;"> <b>Total Penerimaan</b> </td>
							<td style="border-bottom: 1px solid black;border-top: 1px solid black;"> : Rp</td>
							<td  style="border-bottom: 1px solid black;border-top: 1px solid black;" colspan="3"><?php echo toFormatMoney($total_penerimaan); ?></td>
						</tr>
					</table>
				</td>
				<td colspan="2">
					<table class="custom-table2" width="100%" border="0">
						<tr>
							<td>Potongan Absensi  </td>
							<td>: Rp</td>
							<td><?php echo toFormatMoney($data->potongan_absensi); ?></td>
						</tr>
						<tr>
							<td>Angsuran Pinj. Lain-lain  </td>
							<td>: Rp</td>
							<td ><?php echo toFormatMoney($data->angsuran_pinjaman); ?> </td>
						</tr>
						<tr>
							<td>Simpanan Koperasi  </td>
							<td>: Rp</td>
							<td><?php echo toFormatMoney($data->simpanan_koperasi); ?></td>
						</tr>
						<tr>
							<td>Pinjaman Koperasi  </td>
							<td>: Rp</td>
							<td> <?php echo toFormatMoney($data->pinjaman_koperasi); ?></td>
						</tr>
						<tr>
							<td>Iuran JHT  </td>
							<td>: Rp</td>
							<td><?php echo toFormatMoney($data->iuran_jht); ?></td>
						</tr>
						<tr>
							<td>Iuran Pensiun  </td>
							<td>: Rp</td>
							<td><?php echo toFormatMoney($data->iuran_pensiun); ?></td>
						</tr>
						<!-- batas -->

						<tr>
							<td>Bpjs Kesehatan  </td>
							<td>: Rp</td>
							<td><?php echo toFormatMoney($data->bpjs_kesehatan); ?></td>
						</tr>
						<tr>
							<td>PPH 21  </td>
							<td>: Rp</td>
							<td><?php echo toFormatMoney($data->pph21); ?></td>
						</tr>


						<?php if ($data->sisa_cuti != null ) {?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>


						<?php if ($data->rapel != null ) {?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>

						<?php if ($data->thr != null ) {?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>

						<?php if ($data->bonus != null ) {?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>

						<?php if ($data->uang_makan_rp != null ) {?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>

						<?php if ($data->pph21_dtp != null ) { ?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>

						<?php if ($data->lain_lain != null ) {?>
							<tr>
								<td>&nbsp;</td>
								<td></td>
							</tr>
						<?php } ?>
						
						<tr>
							<td style="border-bottom: 1px solid black;border-top: 1px solid black;"> <b>Total Potongan</b> </td>
							<td style="border-bottom: 1px solid black;border-top: 1px solid black;"> : Rp</td>
							<td  style="border-bottom: 1px solid black;border-top: 1px solid black;"> <?php echo toFormatMoney($total_potongan ); ?></td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- batas -->
			<tr>
				<td> </td>
				<td style="border-right: none; text-align: right;"><b>Total</b></td>
				<td style="border-left: none;border-right: none; text-align: left">Rp. </td>
				<td style="border-left: none;"> <?php echo toFormatMoney($total_penerimaan_potongan ); ?></td>
			</tr>
			<!--  -->

			<tr>
				<td colspan="2"> 
					<table class="custom-table2" width="100%" border="0">
						<tr>
							<td>
								<u><b>Overtime 15 <?php echo getMonthName($data->bulan)." ".$data->tahun; ?></b><u>  <br></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td> Act Jam (Overtime) </td>
									<td>: Rp</td>
									<td ><?php echo toFormatMoney($data->overtime_jam); ?> Jam</td>
								</tr>
								<tr>
									<td> Total Fee  (Overtime) 15 <?php echo getMonthName($data->bulan)." ".$data->tahun; ?> </td>
									<td>: Rp</td>
									<td ><?php echo toFormatMoney($data->overtime); ?></td>
								</tr>
								<tr>
									<td> Total Fee  (Overtime) </td>
									<td>: Rp</td>
									<td ><?php echo toFormatMoney($data->overtime); ?></td>
								</tr>
							</table> 
						</td> 
						<td colspan="2"></td>
					</tr>
					<tr> 
						<td> </td>
						<td style="border-right: none; text-align: right;"><b>Take Home Pay</b></td>
						<td style="border-left: none;border-right: none; text-align: left">Rp. </td>
						<td style="border-left: none;"><?php echo toFormatMoney($take_home_pay ); ?></td>
					</tr>
					<tr>
						<td  style="border-left: none;text-align: left;"><b>Keterangan: </b>  <br> - Salary <?php echo getMonthName($data->bulan)." ".$data->tahun; ?>
						<br>&nbsp;</td>
						<td><br><?php echo $data->cutt_off; ?><br>&nbsp;</td>
						<td  style="border-left: none;text-align: right;"><b>Tanggal Cetak</b></td>
						<td  style="border-left: none;text-align: left;">: <?php echo date('d-M-Y'); ?></td>
					</tr>
				</table>
			</div>
		</body>
		</html>
