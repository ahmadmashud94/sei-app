<style type="text/css">

table{
	font-size: 10px;
	text-align: center;
}
table td{
	width: 100px;
	white-space: nowrap;
}
</style>
<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<i class="pe-7s-notebook icon-gradient bg-happy-itmeo">
					</i>
				</div>
				<div>Data Param
					<div class="page-title-subheading">Data Master param gaji
					</div>
				</div>
			</div>   
		</div>  
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="main-card mb-3 card">
				<div class="card-body">
					<!-- <h5 class="card-title">Data table</h5> -->
					<table  width="100%" id="data-table" class="mb-0 table">
						<thead>
							<tr>
								<th>No</th>
								<th>Deskripsi</th>
								<th style="display:none"></th>
								<th>Nilai</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no = 1;
							foreach ($data as $key) {
								?>
								<tr>
									<form class="form" method="POST" action="<?php echo base_url() ?>Controller_Param/save">
										<td><?php echo $no  ;?></td>
										<td><?php echo $key->description ;?></td>
										<td style="display:none"><input type="hidden" name="key" value="<?php echo $key->key ;?>"/></td>
										<td><input style="text-align:center" type="text" name="value" value="Rp <?php echo toFormatMoney($key->value) ;?>"/></td>
										<td><button type="submit" class="btn btn-primary">Simpan</button></td>
									</form>
								</tr>
								<?php 
								$no++;
							}

							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>