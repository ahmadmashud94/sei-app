

<!-- jquery -->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<!-- //jquery -->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<!-- Necessary-JavaScript-File-For-Bootstrap -->

<script src="<?php echo base_url() ?>assets/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>

<!--table-->
<script src="<?php echo base_url() ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/assets/js/init-scripts/data-table/datatables-init.js"></script>

<script src="<?php echo base_url() ?>assets/js/common.js"></script>
<script src="<?php echo base_url() ?>assets/js/accounting.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/main.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#data-table').DataTable( {
			scrollY:        "300px",
			scrollX:        true,
			scrollCollapse: true,
			fixedColumns:   {
				leftColumns: 1,
				rightColumns: 1
			}
		} );
	} );
</script>

<?php switch($this->uri->segment(1)): 
	case "Controller_Param": ?>
	<script src="<?php echo base_url() ?>assets/assets/js/param.js"></script>
	<?php break; ?>
	<?php case "Controller_Salary": ?>
	<script src="<?php echo base_url() ?>assets/assets/js/salary.js"></script>
	<?php break; ?>
<?php endswitch; ?>

</body>
</html>
