<!doctype html>
    <html lang="en">

    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>APP GAJI</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
        <meta name="description" content="Wide selection of buttons that feature different styles for backgrounds, borders and hover options!">
        <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">



<style type="text/css">
.button-sidebar{
    color: white;
    background-color:#437be3;
    border-radius: 25px;   
    opacity: 1;}
</style>
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow" style="background-color: white">
            <div class="app-header__logo">
                <div class="logo-src" style="background:url(<?php echo base_url(); ?>assets/images/logo.png);height: 36px;
                width: 94px;"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div> 
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                  <div class="logo-src" style="background:url(<?php echo base_url(); ?>assets/images/logo.png);height: 36px;
                width: 94px;"></div>
                    </button>
                </span>
            </div>    
        </div>     
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>    <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            <?php  if ($_SESSION['user_session'][0]->user_type == 0) { ?>
                            <li class="app-sidebar__heading">GAJI</li>
                            <li>
                  <!--               <a  href="<?php echo base_url() ?>Controller_Param">
                                    <i class="metismenu-icon pe-7s-notebook button-sidebar" style=" 
                                    opacity: 1;"></i>
                                    Paremeter
                                </a> -->
                                <a href="<?php echo base_url() ?>Controller_Employee">
                                    <i class="metismenu-icon pe-7s-user button-sidebar" style=" 
                                    opacity: 1;"></i>
                                    Master Karyawan
                                </a>
                                <a  href="<?php echo base_url() ?>Controller_Salary">
                                    <i class="metismenu-icon pe-7s-cash button-sidebar" style=" 
                                    opacity: 1;"></i>
                                    Gaji
                                </a>
                            </li>
                            <?php  } ?>
                            <li class="app-sidebar__heading">AKUN</li>
                            <li>
                                <a href="<?php echo base_url() ?>Controller_Login/logout">
                                    <i class="metismenu-icon pe-7s-back-2 button-sidebar" style=" 
                                    opacity: 1;"></i>
                                    Logout
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>   

            <div class="app-main__outer">
