

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Upload Gaji</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form  enctype="multipart/form-data" id="id-form"  method="POST" action="<?php echo base_url() ?>Controller_Salary/upload_salary">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="showEasing">Jenis File</label>
								<select name="salary_type" class="form-control">
									<option value="first">GAJI AWAL</option>
									<option value="final">GAJI FINAL</option>
								</select>
							</div>
						</div>
						<br>
						<div class="col-md-12">
							<div class="form-group">
								<label >Periode</label>
								<input type="month" class="form-control" name="period">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label >Pilih File</label>
								<input type="file"  class="form-control" name="salary_file">
							</div>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Upload</button>
				</div>
			</form>
		</div>
	</div>
</div>