
<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>SEI system</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login-template.css">
</head>

<body> 
   <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->
        <br>
        <!-- Icon -->
        <div class="fadeIn first">
            <img width="2" height="100" src="<?php echo base_url(); ?>assets/images/logo1.png" id="icon" alt="User Icon" />
        </div>
        <h2  style="color:red"><?php echo @$hasil_login; ?></h2>

        <!-- Login Form -->
        <form  autocomplete="off"  action="<?php echo base_url() ?>Controller_Login/login" method="POST" >
          <input type="text" id="login" class="fadeIn second" name="username" placeholder="<?php echo isMobile() ? 'Nik' : 'Username' ?>">
          <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
          <input type="submit" class="fadeIn fourth" value="Log In">
      </form>

      <!-- Remind Passowrd -->
 <!--      <div id="formFooter">
          <a class="underlineHover" href="#">Forgot Password?</a>
      </div> -->

  </div>
</div>

</body>

</html>