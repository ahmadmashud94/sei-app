<style type="text/css">

table{
	font-size: 10px;
	text-align: center;
}
table td{
	width: 100px;
	white-space: nowrap;
}
</style>
<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<i class="pe-7s-user icon-gradient bg-happy-itmeo">
					</i>
				</div>
				<div>Data Karyawan
					<div class="page-title-subheading">Import Data Master Karyawan
					</div>
				</div>
			</div>
			<div class="page-title-actions">
				<button type="button" class="btn mr-2 mb-2 btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Upload Data Karyawan</button><br>
				<a target="_blank"  href="<?php echo base_url(); ?>assets/template/template upload karyawan.xlsx">Download Template Upload</a>
			</div>    
		</div>  
	</div>
	<br>    

	<div class="row">
		<div class="col-lg-12">
			<div class="main-card mb-3 card">
				<div class="card-body">
					<!-- <h5 class="card-title">Data table</h5> -->
					<table  width="100%" id="data-table" class="mb-0 table">
						<thead>
							<tr>

								<th>No</th>
								<th>Nik</th>
								<th>Nama</th>
								<th>Tanggal Lahir</th>
								<th>Agama</th>
								<th>Status Pribadi</th>
								<th>Dept</th>
								<th>Sub Dept</th>
								<th>Jabatan</th>
								<th>Plant</th>
								<th>No Rek</th>
								<th>No Npwp</th>
								<th>Tanggal Masuk</th>
								<th>Status</th>
								<th>Sisa Cuti</th>
								<th>Saldo Tabungan</th>
								<th>Sisa Pinjaman</th>
								<th>Masa Kerja</th>
								<th></th>

							</tr>
						</thead>
						<tbody>
							<?php 
							$no = 1;
							foreach ($data as $key) {
								?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $key->nik ;?></td>
									<td><?php echo $key->nama ;?></td>
									<td><?php echo $key->tanggal_lahir ;?></td>
									<td><?php echo $key->agama ;?></td>
									<td><?php echo $key->status_pribadi ;?></td>
									<td><?php echo $key->dept ;?></td>
									<td><?php echo $key->sub_dept ;?></td>
									<td><?php echo $key->jabatan ;?></td>
									<td><?php echo $key->plant ;?></td>
									<td><?php echo $key->no_rek ;?></td>
									<td><?php echo $key->no_npwp ;?></td>
									<td><?php echo $key->tanggal_masuk ;?></td>
									<td><?php echo $key->status ;?></td>
									<td><?php echo $key->sisa_cuti ;?></td>
									<td>Rp <?php echo toFormatMoney($key->saldo_tabungan) ;?></td>
									<td>Rp <?php echo toFormatMoney($key->sisa_pinjaman) ;?></td>
									<?php 
									$tanggal_masuk= stringToDate($key->tanggal_masuk);
									$tanggal_sekarang = date_create();
									$masa_kerja = floor(date_diff($tanggal_masuk,$tanggal_sekarang)->format("%R%a")/365);
									?>
									<td><?php echo $masa_kerja ;?> Tahun</td>
									<td><a style="color:white" class="btn btn-danger"  href="<?php echo base_url() ?>Controller_Employee/delete/<?php echo $key->nik; ?>">Hapus</a></td>
								</tr>
								<?php 
							}

							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>