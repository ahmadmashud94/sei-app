<style type="text/css">
 
table{
	font-size: 10px;
	text-align: center;
}
table td{
	width: 100px;
	white-space: nowrap;
}
</style>
<div class="app-main__inner">

	<div class="row ">
		<div class="col-lg-12">
			<div class="main-card col-lg-6 card">
				<div class="card-body">
					<h5 class="card-title">Periode Gaji</h5>
					<form method="GET" action="<?php echo base_url(); ?>Controller_Salary">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="showEasing">Pilih Bulan</label>
									<input id="showEasing" type="month" class="form-control" name="period" 
									value="<?php echo $period; ?>">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-success">CARI</button>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<br>
									<div class="page-title-actions">
										<button type="button" class="btn mr-2 mb-2 btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Upload Gaji</button><br>
										<a  target="_blank"  href="<?php echo base_url(); ?>assets/template/template upload gaji2.xlsx">Download Template Upload</a>
									</div>   

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>    
	<br>

	<div class="row">
		<div class="col-lg-12">
			<div class="main-card mb-3 card">
				<div class="card-body">
					<!-- <h5 class="card-title">Gaji Karyawn Bulan </h5> -->
					<table  width="100%" id="data-table" class="mb-0 table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>NIK</th>
								<th>Hadir</th>
								<th>Gaji Pokok</th>
								<th>Tunjangan Kehadiran</th>
								<th>Shift</th>
								<th>Tunjangan Jabatan</th>
								<th>Uang Makan</th>
								<th>Uang Makan Rp</th>
								<th>Insentif Kehadiran</th>
								<th>Potongan Absensi</th>
								<th>Overtime Jam</th>
								<th>Overtime</th>
								<th>THR</th>
								<th>Bonus</th>
								<th>BPJS Kesehatan</th>
								<th>Iuran JHT</th>
								<th>Iuran Pensiun</th>
								<th>Simpanan Koperasi</th>
								<th>Pinjaman Koperasi</th>
								<th>Angsuran Pinj. Lain-lain</th>
								<th>Faktor Penambah</th>
								<th>Faktor Pengurang</th>
								<th>PPH21 DTP</th>
								<th>PPH21</th>
								<th>Cutt Off</th>
								<th>Rapel Kenaikan (Juli s/d Okt)</th>
								<th>Sisa Cuti Tahun Sebelumnya</th>
								<th>Lain-lain</th>
								<th>Cetak Slip Gaji</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no = 1;
							foreach ($data as $key) {
								?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $key->nama;  ?></td>
									<td><?php echo $key->nik;  ?></td>
									<td><?php echo $key->hadir;  ?></td>
									<td>Rp <?php echo toFormatMoney($key->gaji_pokok);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->tunjangan_kehadiran_transport);  ?></td>
									<td><?php echo $key->shift;  ?></td>
									<td>Rp <?php echo toFormatMoney($key->tunjangan_jabatan);  ?></td>
									<td><?php echo toFormatMoney($key->uang_makan);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->uang_makan_rp);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->insentif_kehadiran);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->potongan_absensi);  ?></td>
									<td><?php echo $key->overtime_jam;  ?> Jam</td>
									<td>Rp <?php echo toFormatMoney($key->overtime);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->thr);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->bonus);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->bpjs_kesehatan);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->iuran_jht);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->iuran_pensiun);  ?></td>  
									<td>Rp <?php echo toFormatMoney($key->simpanan_koperasi);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->pinjaman_koperasi);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->angsuran_pinjaman);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->faktor_penambah);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->faktor_pengurang);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->pph21_dtp);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->pph21);  ?></td>
									<td>Rp <?php echo $key->cutt_off;  ?></td>
									<td>Rp <?php echo toFormatMoney($key->rapel);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->sisa_cuti);  ?></td>
									<td>Rp <?php echo toFormatMoney($key->lain_lain);  ?></td>
									<td><a target="_blank" href="<?php echo base_url(); ?>Controller_Salary/print_slip_gaji?period=<?php echo $key->tahun."-".$key->bulan;  ?>&nik=<?php echo $key->nik;  ?>"><span class="pe-7s-print" style="font-size: 12px;"></span></a></td>
								</tr>
								<?php 
							}

							?>
						</tbody>
					</table> 
				</div>
			</div>
		</div>
	</div>
</div>
