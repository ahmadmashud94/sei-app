<?php
class model_param extends ci_model{

	public function __construct() 
	{
		parent::__construct(); 
		$this->load->database();
	}
	public function get_all(){
		$query= "SELECT * FROM tbl_param";
		return $this->db->query($query)->result();
	}
	function save($data,$key)
	{
		$this->db->where('key', $key);
		$this->db->update('tbl_param', $data);
		return true;
	}
}