<?php
class model_employee extends ci_model{

	public function __construct() 
	{
		parent::__construct(); 
		$this->load->database();
	}
	public function get_all(){
		$query= "SELECT * FROM tbl_karyawan";
		return $this->db->query($query)->result();
	}
	function save($data)
	{
		$this->db->insert_on_duplicate_update_batch('tbl_karyawan', $data);
		return true;
	}

	public function get_karyawan_by_nik($nik){
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->where_in('nik', $nik);
		return $this->db->get()->row();
	}
	
	public function get_karyawan_by_niks($niks){
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->where_in('nik', $niks);
		return $this->db->get()->result();
	}


	function delete($nik)
	{
		$status_save =false;
		$this->db->where('nik', $nik);
		$this->db->delete('tbl_karyawan');
		$this->db->where('nik', $nik);
		$this->db->delete('tbl_gaji');
		$this->db->where('username', $nik);
		$this->db->delete('tbl_user');
		return true;
	}
}