	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_Salary extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('Model_Salary');
			$this->load->model('Model_Employee');
			$this->load->model('Model_Param');
			$this->load->library('SimpleXLSX');
			cek_session();

		}  
		public function index(){
			$data['data'] = $this->Model_Salary->get_all(@$_GET['period']);
			$data['period'] = @$_GET['period'];
			$this->load->view('template/sidebar');
			$this->load->view('salary.php',$data);
			$this->load->view('template/footer');
			$this->load->view('modal/salary_upload.php');
		} 

		function print_slip_gaji(){
			$this->load->helper('dompdf');
			$data['data'] = $this->Model_Salary->get_data_by_nik_and_period($_GET['nik'],$_GET['period']);
			if ($data['data'] != null) {
				$html = $this->load->view('print_salary_slip_2',$data,true);
				// if ($data['data']->status == 'PKWT 1' || $data['data']->status == 'PKWT_1') {
				// 	$html = $this->load->view('print_salary_slip_1',$data,true);
				// }else{
				// 	$html = $this->load->view('print_salary_slip_2',$data,true);
				// }
				$filename = "Slip";
				$paper = 'A4';
				$orientation = 'landscape';
				pdf_create($html, $filename, $paper, $orientation, 'Print Slip Gaji');
			}
			$data['heading'] = 'Cetak Gaji';
			$data['message'] = 'Data tidak ditemukan';
			$this->load->view('errors/html/error_404.php',$data);
		}

		public function upload_salary(){
			$allowed = array('xlsx');
			$filename = $_FILES['salary_file']['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if (!in_array($ext, $allowed)) {
				$this->load->view('errors/html/error_general.php',toExepctionData("Error upload Gaji","Pastikan file yang anda upload benar dan sesuai format"));
				return;
			}
			$path = $_FILES["salary_file"]["tmp_name"];
			$salar_type = $_POST['salary_type'];
			$tahun = getYearOfPeriod($_POST['period']);
			$bulan = getMonthOfPeriod($_POST['period']);
			$xlsx = new SimpleXLSX( $path );
			$data_array;
			$nik_array;
			$values;
			$data_aray_final;
			
			if ($salar_type == "first") {
				$values = $xlsx->rows(0);
				if (count($values[2]) == 27) {
					unset($values[0],$values[1],$values[2]);
					foreach ($values  as $key => $value) {
						$x = 1;
						$nik_array[] = $value[$x];

						$nik = $value[$x++];
						$gaji_pokok = $value[$x++];
						$tunjangan_karyawan = $value[$x++];
						$tunjangan_masa_kerja = $value[$x++];
						$tunjangan_kehadiran_transport = $value[$x++];
						$tunjangan_shift = $value[$x++];
						$tunjangan_jabatan = $value[$x++];
						$uang_makan = $value[$x++];
						$potongan_absensi = $value[$x++];
						$insentif_kehadiran = $value[$x++];
						$overtime_jam = $value[$x++];
						$overtime = $value[$x++];
						$thr = !is_numeric($value[$x]) ? null : $value[$x] ;
						$x++;
						$bonus = !is_numeric($value[$x]) ? null : $value[$x] ;
						$x++;
						$bpjs_kesehatan = $value[$x++];
						$iuran_jht = $value[$x++];
						$iuran_pensiun = $value[$x++];
						$simpanan_koperasi = $value[$x++];
						$pinjaman_koperasi = $value[$x++];
						$angsuran_pinjaman = $value[$x++];
						$faktor_penambah = $value[$x++];
						$faktor_pengurang = $value[$x++];
						$cutt_off = $value[$x++];
						$rapel = !is_numeric($value[$x]) ? null : $value[$x] ;
						$x++;
						$sisa_cuti = !is_numeric($value[$x]) ? null : $value[$x] ;
						$x++;
						$lain_lain = !is_numeric($value[$x]) ? null : $value[$x] ;
						$x++;

						$data_array[] = array(
							'nik' => $nik,
							'bulan' => $bulan,
							'tahun' => $tahun,
							'gaji_pokok' => $gaji_pokok,
							'tunjangan_karyawan' => $tunjangan_karyawan,
							'tunjangan_masa_kerja' => $tunjangan_masa_kerja,
							'tunjangan_kehadiran_transport' => $tunjangan_kehadiran_transport,
							'tunjangan_shift' => $tunjangan_shift,
							'tunjangan_jabatan' => $tunjangan_jabatan,
							'uang_makan_rp' => $uang_makan,
							'potongan_absensi' => $potongan_absensi,
							'insentif_kehadiran' => $insentif_kehadiran,
							'overtime_jam' => $overtime_jam,
							'overtime' => $overtime,
							'thr' => $thr, //->> rapel kenaikan Upah (Juli s/d Okt)
							'bonus' => $bonus,
							'bpjs_kesehatan' => $bpjs_kesehatan,
							'iuran_jht' => $iuran_jht,
							'iuran_pensiun' => $iuran_pensiun,
							'simpanan_koperasi' => $simpanan_koperasi,
							'pinjaman_koperasi' => $pinjaman_koperasi,
							'angsuran_pinjaman' => $angsuran_pinjaman,
							'faktor_penambah' => $faktor_penambah,
							'faktor_pengurang' => $faktor_pengurang,
							'cutt_off' => $cutt_off,
							'rapel' => $rapel,
							'sisa_cuti' => $sisa_cuti,
							'lain_lain' => $lain_lain
						);
					}
					$this->Model_Salary->save($data_array);
				}else{
					echo "Pastikan file yang anda upload benar dan sesuai format";
					die;
				}

			}else{
					//final salary
				$values = $xlsx->rows(1);
				$map_data;
				if (count($values[2]) == 5) {
					unset($values[0],$values[1],$values[2]);

					foreach ($values  as $key => $value) {
						$nik = $value[2];
						$pph21_dtp = $value[3] == "" ? null : $value[3];
						$pph21 = $value[4];
						$map_data[$nik] = array(
							'nik' => $nik,
							'tahun' => $tahun,
							'bulan' => $bulan,
							'pph21_dtp' => $pph21_dtp,
							'pph21' => $pph21
						);
					}
					$this->Model_Salary->save_final($map_data,$bulan,$tahun);
				}else{
					echo "Pastikan file yang anda upload benar dan sesuai format";
					die;
				}
			}
			redirect('/Controller_Salary', 'refresh');
		}

		function get_param(){
			$data_param = $this->Model_Param->get_all();
			return arrayToMap($data_param,"key");
		}

		//FOR ANDROID

		public function view_user(){
			$data['data'] = $this->Model_Employee->get_karyawan_by_nik($_SESSION['user_session'][0]->username);
			$this->load->view('template/sidebar');
			$this->load->view('salary_user.php',$data);
			$this->load->view('template/footer');
		}

		
		function print_preview(){
			$data['data'] = $this->Model_Salary->get_data_by_nik_and_period($_GET['nik'],$_GET['period']);
			$this->load->view('template/sidebar');
			$this->load->view('print_preview.php',$data);
			$this->load->view('template/footer');
		}
		
	}