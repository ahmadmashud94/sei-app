	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	use PhpSpreadsheet\IOFactory;
	class Controller_Param extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('Model_Param');
			cek_session();

		} 

		public function index(){
			$data['data'] = $this->Model_Param->get_all();
			$this->load->view('template/sidebar');
			$this->load->view('param.php',$data);
			$this->load->view('template/footer');
			// $this->load->view('modal/param.php');
		}

		public function save(){
			$key = $_POST['key'];
			$data = array(
				'value'=>preg_replace('/[^\d,\,]/', '',$_POST['value'])
			);
			$this->Model_Param->save($data,$key);
			redirect('/Controller_Param','refresh');
		}


	}