<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Model_Login');

	}
	public function index(){
		$this->load->view('login.php');
	}
	public function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data['loggin'] = $this->Model_Login->login($username,$password);
		if ($data['loggin']) {
			if ($_SESSION['user_session'][0]->user_type == 0) {
				redirect('/Controller_Home', 'refresh');
			}else{
				redirect('/Controller_Salary/view_user', 'refresh');	
			}
		}else{
			$data['hasil_login'] = 'Username/Password tidak valid';
			$this->load->view('login',$data);
		}
	}
	public function logout(){
		$this->session->unset_userdata('user_session');
		redirect('/Controller_Login', 'refresh');
	}

}