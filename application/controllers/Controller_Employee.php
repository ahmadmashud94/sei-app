	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	use PhpSpreadsheet\IOFactory;
	class Controller_Employee extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('Model_Employee');
			$this->load->model('Model_User');
			$this->load->library('SimpleXLSX');
			cek_session();

		} 

		public function index(){
			$data['data'] = $this->Model_Employee->get_all();
			$this->load->view('template/sidebar');
			$this->load->view('employee.php',$data);
			$this->load->view('template/footer');
			$this->load->view('modal/employee_upload.php');
		}

		public function upload_employee(){
			$allowed = array('xlsx');
			$filename = $_FILES['employee_file']['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if (!in_array($ext, $allowed)) {
				$this->load->view('errors/html/error_general.php',toExepctionData("Error upload data karyawan","Pastikan file yang anda upload benar dan sesuai format"));
				return;
			}
			$path = $_FILES["employee_file"]["tmp_name"];
			$xlsx = new SimpleXLSX( $path );
			$values = $xlsx->rows(0);
			if (count($values[2]) == 18) {
				unset($values[0],$values[1],$values[2],$values[3]);
				$data_array;
				$data_user;
				foreach ($values  as $key => $value) {

					$nik = $value[1];
					$nama = $value[2]; 
					$tanggal_lahir = $value[3]; 
					$agama = $value[4];
					$status_pribadi = $value[5]; 
					$dept = $value[6];
					$sub_dept = $value[7];
					$jabatan = $value[8];
					$plant = $value[9];
					$no_rek = $value[10];
					$no_npwp = $value[11];

					$tanggal_masuk = $value[12];
					$status = $value[13];
					$saldo_tabungan = $value[14];
					$sisa_pinjaman = $value[15];
					$sisa_cuti = $value[16];
					$password = $value[17];

					$data_user[] = array(
						'username'=>$nik,
						'nama'=>$nama,
						'password'=>$password
					);

					$data_array[] = array(
						'nik'=>$nik,
						'nama'=>$nama,
						'tanggal_lahir'=>$tanggal_lahir,
						'agama'=>$agama,
						'status_pribadi'=>$status_pribadi,
						'dept'=>$dept,
						'sub_dept'=>$sub_dept,
						'jabatan'=>$jabatan,
						'plant'=>$plant,
						'no_rek'=>$no_rek,
						'no_npwp'=>$no_npwp,
						'tanggal_masuk'=>$tanggal_masuk,
						'status'=>$status,
						'saldo_tabungan'=>$saldo_tabungan,
						'sisa_pinjaman'=>$sisa_pinjaman,
						'sisa_cuti'=>$sisa_cuti
					);
				}
				$this->Model_Employee->save($data_array);
				$this->Model_User->save($data_user);
				redirect('/Controller_Employee','refresh');
			}else{
				$data['heading'] = "Error upload data karyawan";
				$data['message'] = "Pastikan file yang anda upload benar dan sesuai format";
				$this->load->view('errors/html/error_general.php',$data);
			}
		}

		public function delete($nik){
			$this->Model_Employee->delete($nik);
			redirect('/Controller_Employee', 'refresh');
		}

	}