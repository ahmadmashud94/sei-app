<?php 
function cek_session(){

	if (@$_SESSION['user_session']  == NULL) {

		redirect('/Controller_Login');

	}
	if (time() - $_SESSION['CREATED'] > 6000000) {
		$_SESSION['user_session'] = null;
		$_SESSION['CREATED'] = null;
		$_SESSION['karyawan_data'] = NULL;
		redirect('/Controller_Login/session_timeout');
	}else{
		$_SESSION['CREATED'] = time(); 
	}
}

function arrayToMap($data,$key_id){
	$data_map;
	foreach ($data as $key => $value) {
		$data_map[$value->$key_id] = $value;
	}
	return $data_map;
}

function dateOfYearMonth($year, $month){
	return date_create($year."/".$month."/"."20");
}

function stringToDate($date){
	return date_create($date);
}

function getMonthOfPeriod($period){
	return $period = explode("-", $period)[1];
}

function getYearOfPeriod($period){
	return $period = explode("-", $period)[0];
}


function getMonthName($month){
	switch ($month) {
		case 1:
		return 'Januari';
		case 2: 
		return 'februari';
		case 3: 
		return 'Maret';
		case 4: 
		return 'April';
		case 5: 
		return 'Mei';
		case 6: 
		return 'Juni';
		case 7: 
		return 'Juli';
		case 8: 
		return 'Agustus';
		case 9: 
		return 'September';
		case 10: 
		return 'Oktober';
		case 11: 
		return 'November';
		case 12: 
		return 'Desember'; 
		default:
		return 'salah';
		break;
	}

}
function encode_img_base64( $img_path = false, $img_type = 'png' ){

	if( $img_path ){
        //convert image into Binary data
		$img_data = fopen( $img_path, 'rb' );
		$img_size = filesize( $img_path );
		$binary_image = fread ( $img_data, $img_size );
		fclose ( $img_data );

        //Build the src string to place inside your img tag
		$img_src = "data:image/".$img_type.";base64,".str_replace ("\n", "", base64_encode($binary_image));

		return $img_src;
	}

	return false;
}

function toFormatMoney($number){
	return number_format($number, 0, '', '.');
}

function toExepctionData($title,$message){
	$data['heading'] = $title;
	$data['message'] = $message;
	return $data;
}

function isMobile(){
	$useragent=$_SERVER['HTTP_USER_AGENT'];

	return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
}
?>